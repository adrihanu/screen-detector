#! /usr/bin/python3
import cv2, mss, numpy, subprocess
from argparse import ArgumentParser
from PIL import Image

def getMouseID(device="mouse"):
    mouseID=subprocess.check_output("xinput --list | grep -i -m 1 '"+device+"' | grep -o 'id=[0-9]\+' | grep -o '[0-9]\+'",shell=True).strip().decode("utf-8")
    return mouseID

def getMouseLocation(): 
    output=subprocess.check_output(["xdotool", "getmouselocation"]).split(b' ')
    x=int(output[0].strip(b'x:'))
    y=int(output[1].strip(b'y:'))
    return x, y                          
 
def getMouseInfo(mouseID):
    info=subprocess.check_output(["xinput", "--query-state", mouseID]).decode("utf-8").replace('\t',' ').replace('\n',' ').split(' ')
    info=[i for i in info if '=' in i]
    info=[i.split('=') for i in info]
    info={key: value for (key, value) in info}
    return info

def setROI(device="mouse"):
    ROI=False
    while True:
        mouseLocation=getMouseLocation()
        mouseInfo = getMouseInfo(getMouseID(device))
        if not ROI and mouseInfo['button[1]']=='down':#first click
            ROI={'left': mouseLocation[0], 'top': mouseLocation[1], 'width': 100, 'height': 100}
        elif ROI and mouseInfo['button[1]']=='down':#update
            width = mouseLocation[0] - ROI['left'] 
            height = mouseLocation[1] - ROI['top']
            ROI['width'] = width if width>0 else 1
            ROI['height'] = height if height>0 else 1
        elif ROI and mouseInfo['button[1]']=='up': break   
        
        if ROI:
            image = numpy.array(sct.grab(ROI))   
            cv2.imshow('image', image)# Display the picture
            cv2.waitKey(1)
    return ROI

if __name__ == '__main__':
    parser = ArgumentParser(description='Program monitors selected region of the screen and detects blue quadrangles. To select a region of screen you want to monitor, press and hold left mouse on top left corner of region and then move your mouse.')
    parser.add_argument('device', choices=['mouse','touchpad'], help="select type of device")
    args = parser.parse_args()

    with mss.mss() as sct:
        ROI=setROI(args.device);
        while True:
            image = numpy.array(sct.grab(ROI))
            hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            # define range of blue color in HSV
            lower_blue = numpy.array([100,180,20])
            upper_blue = numpy.array([135,255,255])
            # Threshold the HSV image to get only blue colors
            mask = cv2.inRange(hsv, lower_blue, upper_blue)
            cnts, h = cv2.findContours(mask, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

            for c in cnts:
                peri = cv2.arcLength(c, True)
                if peri < 100: continue
                approx = cv2.approxPolyDP(c, 0.04 * peri, True)
                if len(approx) == 4: 
                    cv2.drawContours(image, [approx], -1, (0, 0, 255), 2)

            cv2.imshow('image', image)# Display the picture
            cv2.waitKey(10)
