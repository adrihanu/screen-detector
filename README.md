# screen-detector

Program monitors selected region of the screen and detects blue quadrangles.

Tags: `Bash`, `Python`, `OpenCV`, `NumPy`

## Running

1. Make sure `xinput` and `xdotool` are installed on your machine: `sudo apt install xinput xdotool`
2. Install all Python packages: `pipenv install`
3. Run the program: `pipenv run python detector.py touchpad`
